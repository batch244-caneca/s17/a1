let yourFullName = prompt("What is your name?");
console.log("Hello, " + yourFullName);

let yourAge = prompt("How old are you?");
console.log("You are " + yourAge + " years old.");

let yourLocation = prompt("Where do you live?");
console.log("You live in " + yourLocation);

function yourDetails(){
	alert('Thank you for your input!');
};
yourDetails();

function topBandArtist(){
	console.log("1. Lany");
	console.log("2. Lauv");
	console.log("3. December Avenue");
	console.log("4. Charlie Puth");
	console.log("5. The Red Jumsuit Apparatus");
};
topBandArtist();

function top5movies(){
	console.log("1. Sgt. Stubby: An American Hero");
	console.log("Rotten Tomatoes Rating: 89%");
	console.log("2. Twilight");
	console.log("Rotten Tomatoes Rating: 49%");
	console.log("3. Matilda");
	console.log("Rotten Tomatoes Rating: 90%");
	console.log("4. To All the Boys I've Loved Before");
	console.log("Rotten Tomatoes Rating: 96%");
	console.log("5. Sierra Burgess is A Loser");
	console.log("Rotten Tomatoes Rating: 61%");
};
top5movies();


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();